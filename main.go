package main

import (
	"github.com/paypal/gatt"
	log "github.com/sirupsen/logrus"
	"gopkg.in/alecthomas/kingpin.v2"

	"os"
	"os/exec"
)

var done = make(chan struct{})

var icon2color = map[string]string{
	"clear-day":           "FFFF00", //yellow
	"clear-night":         "000000", //black
	"rain":                "0000FF", //blue
	"snow":                "FFFFFF", //white
	"sleet":               "000080", //navy
	"wind":                "C0C0C0", //silver
	"fog":                 "008080", //teal
	"cloudy":              "808080", //grey
	"partly-cloudy-day":   "00FFFF", //aqua
	"partly-cloudy-night": "808000", //olive
}

var (
	apikey = kingpin.Flag("apikey", "Dark Sky API Key.").Short('k').Envar("DARK_SKY_KEY").String()
	debug  = kingpin.Flag("debug", "Debug output.").Short('d').Bool()
	lat    = kingpin.Flag("lat", "Latitude.").Short('l').Float()
	lon    = kingpin.Flag("lon", "Longitude.").Short('o').Float()
	name   = kingpin.Flag("name", "name of device to look for.").Short('n').Default("LightMug").String()
	sleep  = kingpin.Flag("sleep", "Second to sleep between forecast requests.").Short('s').Default("600s").Duration()
)

func main() {
	runWithGodebug()
	kingpin.Parse()
	log.SetOutput(os.Stdout)
	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	d, err := gatt.NewDevice(gatt.MacDeviceRole(gatt.CentralManager))
	if err != nil {
		log.Fatalf("Failed to open device, err: %s\n", err)
		return
	}

	// Register handlers.
	d.Handle(
		gatt.PeripheralDiscovered(onPeriphDiscovered),
		gatt.PeripheralConnected(onPeriphConnected),
		gatt.PeripheralDisconnected(onPeriphDisconnected),
	)
	d.Init(onStateChanged)
	<-done
	log.Debug("Done")
}

/*
	To prevent runtime error "cgo argument has Go pointer to Go pointer",
	this env needs to be set.  Since the program is run as a one-off, I'm embedding
	this as the garbage collection issue it represents should be a non-issue.
	This code from https://gist.github.com/CyrusRoshan/6a5283492e9f60dd4a655c0eba54760f
	Gatt error discussed here: https://github.com/paypal/gatt/issues/76
*/
func runWithGodebug() {
	if os.Getenv("GODEBUG") != "cgocheck=0" {
		cmd := exec.Command(os.Args[0], os.Args[1:]...)
		env := os.Environ()
		env = append(env, "GODEBUG=cgocheck=0")
		cmd.Env = env
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err != nil {
			log.Error(err)
		}
		os.Exit(0)
	}
}

func onStateChanged(d gatt.Device, s gatt.State) {
	log.Debugf("State: %v", s)
	switch s {
	case gatt.StatePoweredOn:
		log.Debug("scanning...")
		d.Scan([]gatt.UUID{RBL_SERVICE_UUID}, false)
		return
	default:
		d.StopScanning()
	}
}

func onPeriphDiscovered(p gatt.Peripheral, a *gatt.Advertisement, rssi int) {
	if *name == "" || *name == a.LocalName {
		p.Device().StopScanning()
		log.Infof("Connecting to %v", a.LocalName)
		p.Device().Connect(p)
	}
}

func onPeriphConnected(p gatt.Peripheral, err error) {
	log.Debug("Connected")
	lightmug := LightMug{peripheral: p}
	lightmug.Init()
}

func onPeriphDisconnected(p gatt.Peripheral, err error) {
	log.Info("Disconnected")
	close(done)
}
