package main

import (
	"encoding/hex"
	"strconv"
	"strings"
	"time"

	forecast "github.com/mlbright/forecast/v2"
	"github.com/paypal/gatt"
	log "github.com/sirupsen/logrus"
)

const LOW_BATTERY = 50
const OFF = "000000"

var RBL_SERVICE_UUID, _ = gatt.ParseUUID("6e400001-b5a3-f393-e0a9-e50e24dcca9e")
var RBL_CHAR_RX_UUID, _ = gatt.ParseUUID("6e400002-b5a3-f393-e0a9-e50e24dcca9e")
var RBL_CHAR_TX_UUID, _ = gatt.ParseUUID("6e400003-b5a3-f393-e0a9-e50e24dcca9e")

var BATT_LEVEL_UUID, _ = gatt.ParseUUID("2A1B")
var BATT_SERVICE_UUID, _ = gatt.ParseUUID("180F")

type LightMug struct {
	peripheral gatt.Peripheral

	LEDCharacteristic     *gatt.Characteristic
	BatteryCharacteristic *gatt.Characteristic

	weatherColor chan string
}

func (lm *LightMug) Init() {
	// Discover services
	services, err := lm.peripheral.DiscoverServices([]gatt.UUID{RBL_SERVICE_UUID, BATT_SERVICE_UUID})
	if err != nil {
		log.Debugf("Failed to discover services, err: %s\n", err)
		return
	}

	for _, service := range services {
		switch service.UUID().String() {
		case BATT_SERVICE_UUID.String():
			log.Debug("Found battery service")
			lm.ServiceHandler(service)
		case RBL_SERVICE_UUID.String():
			log.Debug("Found uart service")
			lm.ServiceHandler(service)
		}
	}
	lm.weatherColor = make(chan string)
	go lm.getWeatherColor()
	go lm.sendLightCommands()
}

func (lm *LightMug) ServiceHandler(service *gatt.Service) {
	characteristics, err := lm.peripheral.DiscoverCharacteristics([]gatt.UUID{}, service)
	if err != nil {
		log.Debugf("Failed to discover characteristics, err: %v", err)
		return
	}

	for _, characteristic := range characteristics {
		switch characteristic.UUID().String() {
		case BATT_LEVEL_UUID.String():
			log.Debug("Found battery characteristic")
			lm.BatteryCharacteristic = characteristic
		case RBL_CHAR_RX_UUID.String():
			log.Debug("Found LED characteristic")
			lm.LEDCharacteristic = characteristic
		}
	}
}

func (lm *LightMug) getBatteryLevel() int {
	if lm.BatteryCharacteristic == nil {
		log.Error("BatteryCharacteristic missing")
		return 0
	}
	b, err := lm.peripheral.ReadCharacteristic(lm.BatteryCharacteristic)
	if err != nil {
		log.Errorf("Failed to read characteristic, err: %v", err)
		return -1
	}
	return int(b[0])
}

func (lm *LightMug) sendLightCommands() {
	log.Debug("Starting sendLightCommands")
	for {
		command := formatCommand(<-lm.weatherColor)
		log.Debugf("Sending command: %v", command)
		err := lm.peripheral.WriteCharacteristic(lm.LEDCharacteristic, command, true)
		if err != nil {
			log.Errorf("Failed to send command, err: %v", err)
		}
	}
}

func (lm *LightMug) getWeatherColor() {
	log.Debug("Starting getWeatherColor")
	for {
		battery := lm.getBatteryLevel()
		if battery < LOW_BATTERY {
			log.Infof("Low battery [%v].  Sending 'OFF'", battery)
			lm.weatherColor <- OFF
			time.Sleep(*sleep * 2)
			continue
		}

		when := "now"
		f, err := forecast.Get(strings.TrimSpace(*apikey), s(*lat), s(*lon), when, forecast.US, forecast.English)
		if err != nil {
			log.Fatalf("Error getting forcast: %v", err)
			return
		}

		log.Infof("%s (%v)", f.Currently.Summary, f.Currently.Temperature)

		if c, ok := icon2color[f.Currently.Icon]; ok {
			lm.weatherColor <- c
		}

		log.Debugf("Sleeping %v", *sleep)
		time.Sleep(*sleep)
	}
}

func formatCommand(color string) []byte {
	var command = []byte{}
	var th byte = 0
	var tl byte = 10

	if color != "" {
		rgb, err := hex.DecodeString(color)
		if err != nil || len(rgb) != 3 {
			log.Debug("Invalid color code format")
			return command
		}

		var led byte = 0 //All
		command = []byte{1, 'c', rgb[0], rgb[1], rgb[2], th, tl, led}
	}

	return command
}

func s(f float64) string {
	return strconv.FormatFloat(f, 'f', 5, 64)
}
